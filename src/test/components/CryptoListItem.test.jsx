import { describe, it } from 'vitest';
import list from "../../Api/SampleMarketList.json"
import { render, screen } from '../TestHelper';
import CryptoListItem from '../../Components/CryptoListItem';

const cryptoItem = list[0];

function setcurrentToken(){

}

describe("test crypto list item", ()=>{
    it("should disply the crypto name, symbole and price pecentage", ()=>{
        render(<CryptoListItem cryptoItem={cryptoItem} setcurrentToken={setcurrentToken} />)

        expect(screen.getByText(/Bitcoin/)).toBeInTheDocument();
        expect(screen.getByText(/btc/)).toBeInTheDocument();
        expect(screen.getByText(/0.02%/)).toBeInTheDocument();
    })
})