//App.test.tsx
import { describe, it } from 'vitest';
import { render, screen } from './TestHelper';
import Navbar from '../../Components/Navbar';

describe("Navbar component test", ()=>{
    it("should display the navbar header", ()=>{
        render(<Navbar />)
        expect(screen.getByText(/Crypto portfolio/i)).toBeInTheDocument();
    })
})