import { useState } from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Navbar from './Components/Navbar'
import CryptoList from './Pages/CryptoList'

function App() {

  return (
    <Router>
      <Navbar />
      <Routes>
        <Route path='/' element={<CryptoList />} />
      </Routes>
    </Router>
  )
}

export default App
