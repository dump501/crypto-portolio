import React, { useState, useEffect } from 'react'
import SampleList from "../Api/SampleMarketList.json"
import CryptoListItem from '../Components/CryptoListItem'
import LineChart from '../Components/LineChart'
import { getTokensMarket } from '../Api/Api'
const sample = SampleList[0]
const CryptoList = () => {
  const [tokens, settokens] = useState(null);
  const [currentToken, setcurrentToken] = useState(null);


  useEffect(()=>{
    const fetchData = async()=>{
      let response = await getTokensMarket()
      response = await response.json()
      settokens(response);
      setcurrentToken(response[0]);
    }
    fetchData()
  }, [])
  return (
    <div className='crypto_list_page pt-1'>
        <div className="crypto_list">
            {tokens && tokens.map((cryptoItem, i) => <CryptoListItem key={i} cryptoItem={cryptoItem} setcurrentToken={setcurrentToken} />)}
        </div>
        {currentToken && <div className="cryptos_chart">
          <h1>{currentToken.name} 7days chart</h1>
          <LineChart token={currentToken} />
        </div>}
    </div>
  )
}

export default CryptoList
