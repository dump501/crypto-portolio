export default function formatPecentage(value=0.123){
    return value.toFixed(2);
}