import React from 'react'

const Triangle = ({direction}) => {
  return (
    <div className={`triangle-${direction ? direction: "up"}`}></div>
  )
}

export default Triangle
