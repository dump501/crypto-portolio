import React, { useEffect, useState } from "react";
import Chart from "chart.js/auto";
import { Line } from "react-chartjs-2";

const labels = ["January", "February", "March", "April", "May", "June"];

const datas = {
  labels: labels,
  datasets: [
    {
      label: "My First dataset",
      backgroundColor: "rgb(255, 99, 132)",
      borderColor: "rgb(255, 99, 132)",
      data: [0, 10, 5, 2, 20, 30, 45],
    },
  ],
};

const LineChart = ({token}) => {
  
  const prepareChartData = (token)=>{
    return{
      labels: token.sparkline_in_7d.price,
      datasets:[
        {
          label: token.name,
          borderColor: token.price_change_percentage_1h_in_currency > 0 ? "green" : "red",
          data: token.sparkline_in_7d.price,
          color: "white"
        },
      ]
    }
  }
  return (
    <div style={{width: "100%"}}>
      <Line
      options={{
        pointStyle: "dash",
        borderJoinStyle: "round",
        responsive: true,
        scales: {
          x:{
            display: false
          },
          y: {
            ticks: {
              color: "white"
            },
            border: {
              color: "white",
            },
            title: {
              color: "white"
            },
            grid: {
              color: "white"
            }
          }
        }
      }}

      data={prepareChartData(token)} />
    </div>
  );
};

export default LineChart;