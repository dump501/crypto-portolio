import React from 'react'
import Triangle from './Triangle'
import formatPecentage from '../Helpers/Helper'
import LineChart from './LineChart'

const CryptoListItem = ({ cryptoItem, setcurrentToken }) => {
  return (
    <>
      <div className="crypto_list_item" onClick={(e) => setcurrentToken(cryptoItem)}>
        <img className='crypto_item_image' src={cryptoItem.image} />
        <div style={{ width: "100%" }}>
          <div className="crypto_item_header">
            <span className='ml-1'>{cryptoItem.name} | {cryptoItem.symbol}</span>
            <span>+</span>
          </div>
          <div className='crypto_item_details'>
            <span className='d-flex' style={
              { color: cryptoItem.price_change_percentage_1h_in_currency > 0 ? "green" : "red" }
            }>
              ${formatPecentage(cryptoItem.current_price)}
            </span>
            <span className='d-flex' style={
              { color: cryptoItem.price_change_percentage_1h_in_currency > 0 ? "green" : "red" }
            }><Triangle direction={
              cryptoItem.price_change_percentage_1h_in_currency > 0 ? "up" : "down"
            } /> {formatPecentage(cryptoItem.price_change_percentage_1h_in_currency)}%</span>
          </div>
        </div>
      </div>
      <div className='crypto_item_chart'>
        <LineChart token={cryptoItem} />
      </div>
    </>
  )
}

export default CryptoListItem
