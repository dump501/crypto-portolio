import React from 'react'

const Col = ({children, xs, sm, md, lg, xl}) => {
  return (
    <div 
    className={`${xs ? `col-${xs}` : "col-12"} ${sm ? `col-sm-${sm}` : ""} ${md ? `col-md-${md}` : ""} ${lg ? `col-lg-${lg}` : ""} ${xl ? `col-xl-${xl}` : ""}`}>
      {children}
    </div>
  )
}

export default Col
