import React from 'react'

const Container = ({children, fluid, p0}) => {
  return (
    <div className={`${fluid ? "container-fluid": "container"} ${p0?"p0":""}`}>{children}</div>
  )
}

export default Container
