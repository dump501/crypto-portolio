import React from 'react'

const Navbar = () => {
  return (
    <div className='navbar'>
        <div className="navbar_header">Crypto portfolio</div>
        <div className="navbar_content">
            <a className="navbar_content_item">Crypto list</a>
            <a className="navbar_content_item">Favorite crypto</a>
            <a className="navbar_content_item">Login</a>
        </div>
    </div>
  )
}

export default Navbar
