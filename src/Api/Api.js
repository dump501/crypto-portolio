

export async function getTokensMarket() {
    try {
      let response = await fetch(`https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=true&price_change_percentage=1h&locale=en`)
      return response
    } catch (error) {
      console.log(error);
    }
  }